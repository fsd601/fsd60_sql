package com.ts;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HiiController {
	
	@RequestMapping("sayHi")
	public String sayHi(){
		return "Hii From HiController";
	}
	
	@RequestMapping("welcome")
	public String welcome(){
		return "welcome to Hii";
	}
	
	
	

}
