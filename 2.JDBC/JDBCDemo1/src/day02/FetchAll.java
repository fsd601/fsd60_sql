package day02;

import java.sql.*;
import java.util.*;

import com.db.DbConnection;
public class FetchAll {

	public static void main(String[] args) {
		Connection connection = DbConnection.getConnection();
		PreparedStatement preparedStatement = null;
		
		ResultSet resultSet = null;
		String selectQuery = "select * from employee";
		
		try {
			preparedStatement = connection.prepareStatement(selectQuery);
						
			
			
			resultSet = preparedStatement.executeQuery();
			while(resultSet.next()){
				System.out.println("Empid   : " + resultSet.getInt(1));
                System.out.println("EmpName : " + resultSet.getString(2));
                System.out.println("Salary  : " + resultSet.getInt(3));
                System.out.println("Gender  : " + resultSet.getString(4));
                System.out.println("EmailId : " + resultSet.getString(5));
                System.out.println("Password: " + resultSet.getString(6));
                System.out.println();
			}
			
			
			preparedStatement.close();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		finally {
			try {				
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}


	}
}

