package com.ts;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

	@RequestMapping("sayHello")
	public String sayHello() {
		return "Hii From HelloController";
	}

	@RequestMapping("welcome1")
	public String welcome1() {
		return "welcome to HelloController";
	}

}
