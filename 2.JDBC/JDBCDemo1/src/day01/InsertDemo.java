package day01;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import com.db.DbConnection;

public class InsertDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Connection connection = DbConnection.getConnection();
		Statement statement = null;

		int empId = 107;
		String empName = "Utkarsh";
		double salary = 9898.98;
		String gender = "Male";
		String emailId = "utkarsh@gmail.com";
		String password = "123";

		String insertQuery = "insert into employee values (" + empId + ", '" + empName + "', " + salary + ", '" + gender
				+ "', '" + emailId + "', '" + password + "')";

		try {
			statement = connection.createStatement();
			int result = statement.executeUpdate(insertQuery);

			if (result > 0) {
				System.out.println(result + " Record(s) Inserted...");
			} else {
				System.out.println("Record Insertion Failed...");
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		try {
			if (connection != null) {
				statement.close();
				connection.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

}
