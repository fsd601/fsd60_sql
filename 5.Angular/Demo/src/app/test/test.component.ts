import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrl: './test.component.css'
})
export class TestComponent implements OnInit {

  id: number;
  name: string;
  age: number;
  address: any;
  hobbies: any;
  constructor() {
    // alert('component invoked');

    this.id = 101;
    this.name = 'Harsha';
    this.age = 22;

    this.address = {
      streetNo: 101,
      city: 'Hyd',
      state: 'Telangana'
    }

    this.hobbies = ["Reading", "Running", "Cooking", "Eating"]
  }

  ngOnInit() {
    // alert('ngOnInit Invoked...');
  } 

}
